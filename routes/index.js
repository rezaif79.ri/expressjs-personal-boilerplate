const mainRouter = require("./mainRoutes");

function routes(app) {
    app.use("/", mainRouter);
    app.all("*", (req, res) => {
        return res.status(404).json({
            message: "Not Found",
            data: {},
            errors: {}
        })
    });
}

module.exports = routes;
